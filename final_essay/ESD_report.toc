\select@language {english}
\contentsline {chapter}{\numberline {1}Overview}{1}
\contentsline {chapter}{\numberline {2}Registers and buffers}{3}
\contentsline {chapter}{\numberline {3}The Memory}{5}
\contentsline {section}{\numberline {3.1}Modules used}{5}
\contentsline {section}{\numberline {3.2}General behaviour}{7}
\contentsline {section}{\numberline {3.3}Tests performed}{8}
\contentsline {chapter}{\numberline {4}The Architecture}{11}
\contentsline {section}{\numberline {4.1}Premise}{11}
\contentsline {section}{\numberline {4.2}Pipelining: outline}{13}
\contentsline {section}{\numberline {4.3}Pipelining: hazards}{15}
\contentsline {chapter}{\numberline {5}Testing ARM source code}{21}
\contentsline {section}{\numberline {5.1}The Fibonacci test}{21}
\contentsline {section}{\numberline {5.2}The nested vector test}{26}
\contentsline {chapter}{\numberline {6}Conclusion}{31}
\contentsline {section}{\numberline {6.1}Remarks}{31}
\contentsline {section}{\numberline {6.2}Possible enhancements}{31}
