#include <systemc>
#include <iostream>
#include <cstdlib>
#include "Memory_16b_RISC.hpp"
#include "Stimulus.hpp"

using namespace std;

int sc_main(int argc, char* argv[])
{
   //Necessary test signals
    	

   sc_clock clock("clock", 10, 0.5, 5, true);
   sc_signal<sc_bv<1> > wl;
   sc_signal<sc_bv<1> > rl;
   sc_signal<sc_bv<16> > test_data_in;
   sc_signal<sc_bv<16> > test_data_out;
   sc_signal<sc_bv<16> > test_addr;   
   sc_signal<sc_int<16> > num_in;
   sc_signal<sc_int<16> > n_writes;

   unsigned number, nwrites;

   if (argc < 3)
   {
      cout << "Use is [# of writes/reads] [number to be written]" << endl;
      exit(EXIT_FAILURE);
   }
   else
   {
      nwrites=atoi(argv[1]);
      number=atoi(argv[2]);
      num_in=number;
      n_writes=nwrites;
   }

   //Stimulus declaration
   Stimulus Test_Stim("RW_STIM");

   //Debug
   cout << endl << "DEBUG:	Stimulus has been declared!" << endl;

   //Stimulus port mapping
   Test_Stim.clock(clock);
   Test_Stim.wl(wl);
   Test_Stim.rl(rl);
   Test_Stim.test_data_in(test_data_in);
   Test_Stim.test_data_out(test_data_out);
   Test_Stim.test_addr(test_addr);
   Test_Stim.n_writes(n_writes);
   Test_Stim.num_in(num_in);

   //Debug
   cout << endl << "DEBUG:	Stimulus has been mapped!" << endl;


   //test memory declaration
   Memory_16b_RISC Test_RAM("RAM_16");

   //Debug
   cout << endl << "DEBUG:	Memory has been declared!" << endl;

   //test memory port mapping
   Test_RAM.clock(clock);
   Test_RAM.addr(test_addr);
   Test_RAM.read_en(rl);
   Test_RAM.write_en(wl);
   Test_RAM.data_in(test_data_in);
   Test_RAM.data_out(test_data_out);

   //Debug
   cout << endl << "DEBUG:	Memory has been mapped!" << endl;

   //preparing simulation output
   sc_trace_file *wf = sc_create_vcd_trace_file("test_rw");

   //tracing test waveforms
   sc_trace(wf, clock, "clock");
   sc_trace(wf, wl, "wl");
   sc_trace(wf, rl, "rl");
   sc_trace(wf, test_data_in, "DataIn");
   sc_trace(wf, test_addr, "Address");
   sc_trace(wf, test_data_out, "DataOut");
   sc_trace(wf, Test_RAM.MemCell.at(0).dout, "value0");
   sc_trace(wf, Test_RAM.mux_data_in.at(0), "mux_in0");
   sc_trace(wf, Test_RAM.MemCell.at(1).dout, "value1");
   sc_trace(wf, Test_RAM.mux_data_in.at(1), "mux_in1");
   sc_trace(wf, Test_RAM.MemCell.at(2).dout, "value2");
   sc_trace(wf, Test_RAM.mux_data_in.at(2), "mux_in2");
   sc_trace(wf, Test_RAM.MemCell.at(3).dout, "value3");
   sc_trace(wf, Test_RAM.mux_data_in.at(3), "mux_in3");

   //Debug
   cout << endl << "DEBUG:	About to start simulation!" << endl;

   //simulate!
   sc_start(SC_ZERO_TIME);

   //Debug
   cout << endl << "DEBUG:	Simulation has actually progressed!" << endl;

   sc_start();

   //Debug
   cout << endl << "DEBUG:	Simulation ended!" << endl;

   //Debug some more 
   for(unsigned k=0;k<n_writes.read();k++)
      cout << "Contenuto cella n°" << k << " :" << Test_RAM.MemCell.at(k).value.to_uint() << endl;

   sc_close_vcd_trace_file(wf);

   return 0;
}

