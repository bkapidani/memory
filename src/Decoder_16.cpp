//file Decoder_16.cpp
#include <systemc.h>
#include "Decoder_16.hpp"

//#define RAM_SIZE 65536

using namespace std;

void Decoder_16 :: route()
{
   sc_bv<16> tmp_in =dec_in.read();
   sc_int<64> tmp = tmp_in.to_uint();
   
   for (unsigned j=0;j < RAM_SIZE; j++)
   {
      if (j==tmp)
	dec_out[j].write('1');
      else
        dec_out[j].write('0');
   }
   return;
}

