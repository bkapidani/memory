#include <systemc.h>
#include "And_1.hpp"


void And_1 :: Calculate()
{
   sc_bv<1> calc = in1.read() & in2.read();
   out1.write(calc);

   return;
}

