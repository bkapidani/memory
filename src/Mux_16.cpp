//file Mux_16.cpp
#include <systemc.h>
#include "Mux_16.hpp"

//#define RAM_SIZE 65536

using namespace std;

void Mux_16 :: select()
{
   //sel_sig = sel.read();
   sc_uint<16> sel_dec = sel.read().to_uint();
   sc_bv<16> data_sig = in.at(sel_dec).read();
   
   //~ cout << endl << "sel_dec:   " << sel_dec << endl;
   //~ cout << "data_sig:  " << data_sig.to_uint() << endl;
   
   out.write(data_sig);

//   return;
}
