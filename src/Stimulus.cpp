#include <systemc.h>
#include "Stimulus.hpp"

void Stimulus :: StimGen()
{
    unsigned k=0;
    unsigned incr=num_in.read();

   //Debug
   cout << endl << "DEBUG:	StimGen() has been called!" << endl;

   while(true)
   {   
		//write
		rl = 0;
		wl = 1;

		while (k< n_writes.read())
		{
		
			  test_data_in = (sc_bv<16>)incr;
			  test_addr = (sc_bv<16>)k;

			  //Prepare for next cycle
			  k++;
			  incr++;
		
			  //wait(SC_ZERO_TIME);	//combinatorial elaboration

			  //~ cout << "Simulation @ " << sc_time_stamp() << endl;
			  //~ cout << "	clock = " << clock << endl;
			  //~ cout << "	wl = " << wl << endl;
			  //~ cout << "	rl = " << rl << endl;
			  //~ cout << "	Address = " << test_addr << endl;
			  //~ cout << "	DataIn = " << test_data_in << endl;
			  //~ cout << "	DataOut = " << test_data_out << endl << endl;

			  wait();
			
			  //~ cout << "Simulation @ " << sc_time_stamp() << endl;
			  //~ cout << "	clock = " << clock << endl;
			  //~ cout << "	wl = " << wl << endl;
			  //~ cout << "	rl = " << rl << endl;
			  //~ cout << "	Address = " << test_addr << endl;
			  //~ cout << "	DataIn = " << test_data_in << endl;
			  //~ cout << "	DataOut = " << test_data_out << endl << endl;	
		}

		k=0;

		//read what I've written.
		rl = 1;
		wl = 0;
		
		while (k<=n_writes.read()) 	//output is ready with one clock cycle delay!
		{
			  test_data_in = (sc_bv<16>)incr;
			  test_addr = (sc_bv<16>)k;
			  k++;
		
			  //wait(SC_ZERO_TIME);	//combinatorial elaboration
			  
			  //~ cout << "Simulation @ " << sc_time_stamp() << endl;
			  //~ cout << "	clock = " << clock << endl;
			  //~ cout << "	wl = " << wl << endl;
			  //~ cout << "	rl = " << rl << endl;
			  //~ cout << "	Address = " << test_addr << endl;
			  //~ cout << "	DataIn = " << test_data_in << endl;
			  //~ cout << "	DataOut = " << test_data_out << endl << endl;

			  wait();

			  //~ cout << "Simulation @ " << sc_time_stamp() << endl;
			  //~ cout << "	clock = " << clock << endl;
			  //~ cout << "	wl = " << wl << endl;
			  //~ cout << "	rl = " << rl << endl;
			  //~ cout << "	Address = " << test_addr << endl;
			  //~ cout << "	DataIn = " << test_data_in << endl;
			  //~ cout << "	DataOut = " << test_data_out << endl << endl;
		}

        k=0;
		
		sc_stop();
		
		return;
   }
}

