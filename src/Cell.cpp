#include <systemc.h>
#include <iostream>
#include "Cell.hpp"


using namespace std;

void Cell :: CellRead()
{
	dout.write(value);
	return;
}

void Cell :: CellWrite()
{
	value = din.read();
	dout = din.read();
	return;
}

void Cell :: CellAccess()
{
	if (wr_en.read() == '1')
		CellWrite();
	else
		CellRead();

	return;
}

Cell& Cell :: operator=(const Cell &c)
{
	this->value=c.value;
	return *this;
}

