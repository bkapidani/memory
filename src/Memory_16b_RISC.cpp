//file Memory_16b_RISC.cpp
#include <systemc.h>
#include <iostream>
#include "Memory_16b_RISC.hpp"

using namespace std;

void Memory_16b_RISC :: Run()
{
	//Debug
	//cout << endl << "DEBUG:	Run() has been called!" << endl;
	if (read_en.read() == "1")
	{
		cout << endl << "Memory @" << sc_time_stamp() << endl;
		cout << "Address:" << addr << endl;
		cout << "Output: " << data_out << endl << endl;
	}

	return;
}

//Memory_16b_RISC ::~Memory_16b_RISC() 
//{
//	delete dec16;
//}

