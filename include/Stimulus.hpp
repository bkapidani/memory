#ifndef STIMULUS_HPP
#define STIMULUS_HPP

#include <systemc>

SC_MODULE(Stimulus)
{   
   //Inputs and Outputs declaration
   sc_in<sc_bv<16> > test_data_out;
   sc_in<sc_int<16> > num_in;
   sc_in<sc_int<16> > n_writes;
   sc_in<bool> clock;
   sc_out<sc_bv<1> > wl;
   sc_out<sc_bv<1> > rl;
   sc_out<sc_bv<16> > test_data_in;
   sc_out<sc_bv<16> > test_addr;

   //Method Declaration
   void StimGen();

   SC_CTOR(Stimulus)
   {
      SC_THREAD(StimGen);
      sensitive << clock.pos();
   }
   
};

#endif

