#ifndef CELL_HPP
#define CELL_HPP

#include <systemc.h>
#include <iostream>

SC_MODULE(Cell)
{
	//This is a behavioural description of a single static Ram cell.
	//An array of such components will be used to describe the main memory
	//of this architecture.

	//inputs
	sc_in<bool> clock;
	sc_in<sc_bv<1> > wr_en;
	sc_in<sc_bv<1> > re_en;
    sc_in<sc_bv<16> > din;

	//data
	sc_bv<16> value;

	//outputs
	sc_out<sc_bv<16> > dout;

	//methods
	void CellRead();
	void CellWrite();
	void CellAccess();
	Cell& operator=(const Cell &c);

	//constructor
	SC_CTOR(Cell)
	{
		//value = "0000000000000000";
		SC_METHOD(CellAccess);
			sensitive << clock.pos(); // << wr_en << re_en;
			//dont_initialize();	
	}
};

#endif

