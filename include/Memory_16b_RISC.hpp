//file Memory_16b_RISC.hpp
#ifndef MEMORY_16B_RISC
#define MEMORY_16B_RISC

#define RAM_SIZE 512
//#define RAM_SIZE 128

#include <systemc.h>
#include <iostream>
#include "Decoder_16.hpp"
#include "Cell.hpp"
#include "And_1.hpp"
#include "Mux_16.hpp"


SC_MODULE(Memory_16b_RISC){

	//Declaration of the inputs
	sc_in<bool> clock;
	sc_in<sc_bv<16> > addr;				//input address <--- ALU output
	sc_in<sc_bv<16> > data_in;			//input data <--- register file 'src2' output.
	sc_in<sc_bv<1> > write_en;			//memory write enable <--- control unit 'we_dmem' output.
	sc_in<sc_bv<1> > read_en;			//memory read enable <--- control unit 're_dmem' output.

	//Declaration of the outputs
	sc_out<sc_bv<16> > data_out;			//output data ---> Multiplexer_3_2 'in3' input. 

	//Signals Declaration
	sc_vector<sc_signal<sc_bv<1> > > sel_bit;
	sc_vector<sc_signal<sc_bv<1> > > read_line;
    sc_vector<sc_signal<sc_bv<1> > > write_line;
    sc_vector<sc_signal<sc_bv<16> > > mux_data_in;

	//Methods Declaration
    void Run();

	//Submodules Declaration
	sc_vector<Cell> MemCell;
	sc_vector<And_1> read_and;
	sc_vector<And_1> write_and;
	Decoder_16 dec16;
    Mux_16 mux16;

	//Constructor
	SC_CTOR(Memory_16b_RISC) 
	   :   MemCell("M1", RAM_SIZE), read_and("A1", RAM_SIZE), write_and("A2", RAM_SIZE), dec16("DEC"), mux16("MUX")
	{
		//Debug
		cout << endl << "DEBUG:	Entered memory constructor!" << endl;

		sel_bit.init(RAM_SIZE);
		read_line.init(RAM_SIZE);
		write_line.init(RAM_SIZE);
		mux_data_in.init(RAM_SIZE);

		//Decoder gate port map
		//dec16.clock(clock);
		dec16.dec_in(addr);
		dec16.dec_out.bind(sel_bit);

        //Multiplexer gate port map
		//mux16.clock(clock);
      	mux16.sel(addr);
       	mux16.out(data_out);
		mux16.in.bind(mux_data_in);

		for (unsigned i=0; i<RAM_SIZE; i++)
		{
			//Cell port map
			MemCell.at(i).clock(clock);
			MemCell.at(i).wr_en(write_line.at(i));
			MemCell.at(i).re_en(read_line.at(i));
			MemCell.at(i).din(data_in);
			MemCell.at(i).dout(mux_data_in.at(i));

			//Reading AND gate port map
			read_and.at(i).clock(clock);
			read_and.at(i).in1(read_en);
			read_and.at(i).in2(sel_bit.at(i));
			read_and.at(i).out1(read_line.at(i));

			//Writing AND gate port map
			write_and.at(i).clock(clock);
			write_and.at(i).in1(write_en);
			write_and.at(i).in2(sel_bit.at(i));
			write_and.at(i).out1(write_line.at(i));
		}


		SC_METHOD(Run);
		sensitive << clock.pos() << addr << write_en << read_en;
			//dont_initialize();
	}

	//~Memory_16b_RISC();

};//End of Memory Module

#endif
