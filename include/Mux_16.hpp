//file Mux_16.hpp
#ifndef MUX_16_HPP
#define MUX_16_HPP

#define RAM_SIZE 512
//~ #define RAM_SIZE 128

#include <systemc.h>
#include <iostream>

SC_MODULE(Mux_16)
{
	//inputs
	//sc_in<bool> clock;
   	sc_vector<sc_in<sc_bv<16> > > in;
    sc_in<sc_bv<16> > sel;
   	
   	//outputs
   	sc_out<sc_bv<16> > out;
	
	//sc_signal<sc_bv<16> > sel_sig;
	//sc_uint<16> sel_dec;
	//sc_signal<sc_bv<16> > data_sig;

   	void select();

   	SC_CTOR(Mux_16)
	:   in("in"), sel("sel"), out("out")
	{
		//Debug
		cout << endl << "DEBUG:	Entered MUX constructor!" << endl;

		in.init(RAM_SIZE);

      		SC_METHOD(select);
				for (unsigned kk=0; kk<RAM_SIZE;kk++)
					sensitive << in.at(kk);
				sensitive << sel; //<< clock.pos();

				
		//dont_initialize();
   	}

};

#endif
