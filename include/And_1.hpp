#ifndef AND_1_HPP
#define AND_1_HPP

#include <systemc.h>


SC_MODULE(And_1)
{
	//This is a behavioural description of a two-one-bit-inputs AND logic gate.
	//This will be used to enable writing and reading of a single cell of memory.

	//inputs
	sc_in<bool> clock; 			//this component is purely combinatory!
	sc_in<sc_bv<1> > in1; 			// mapped to r or w enable from control unit
	sc_in<sc_bv<1> > in2;			// mapped to i-th decoder exit corresponding to the present cell 

	//outputs
	sc_out<sc_bv<1> > out1;			// mapped to cell we or re inputs

	//methods
	void Calculate();

	//constructor
	SC_CTOR(And_1)
	{
		SC_METHOD(Calculate);
			sensitive << in1 << in2;
			//dont_initialize();	
	}
};

#endif

