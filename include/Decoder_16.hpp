//file Decoder_16.hpp
#ifndef DECODER_16_HPP
#define DECODER_16_HPP

#define RAM_SIZE 512
//~ #define RAM_SIZE 128

#include <systemc.h>
#include <iostream>

SC_MODULE(Decoder_16)
{
	//sc_in<bool> clock;
   	sc_in<sc_bv<16> > dec_in;
   	sc_vector<sc_out<sc_bv<1> > > dec_out;

   	void route();

   	SC_CTOR(Decoder_16) 
	{
		dec_out.init(RAM_SIZE);

      		SC_METHOD(route); 

      		sensitive << dec_in; //<< clock.pos(); 
			//dont_initialize();
   	}

};

#endif

